var rads = 170;
var nodes = 6;
var shells = 40;
var rings = [];

function setup() {
    colorMode(HSB);
    var canvas = createCanvas(700,700);
    var startx = width/2;
    var starty = height/2;
    rings.push(new Circle(startx,starty,rads));

        for(var i = 0;i < nodes+1;i++){
            var x,y;
            x = startx + rads/2 * cos(2 * PI * i / nodes);
            y = starty + rads/2 * sin(2 * PI * i / nodes);
            rings.push(new Circle(x,y,rads));

            for(var j = 0;j < nodes+1;j++){
                var xj,yj;
                xj = x + rads/2 * cos(2 * PI * j / nodes);
                yj = y + rads/2 * sin(2 * PI * j / nodes);
                rings.push(new Circle(xj,yj,rads));

                for(var k = 0;k < nodes+1;k++){
                    var xk,yk;
                    xk = xj + rads/2 * cos(2 * PI * k / nodes);
                    yk = yj + rads/2 * sin(2 * PI * k / nodes);
                    rings.push(new Circle(xk,yk,rads));
                }
            }
        }
    }


function draw(){
    background(0,0,0,0.08);
    for(var i = 0;i<rings.length-1;i++){
        rings[i].teken();
    }
}

class Circle {
    constructor (x,y,r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }
    teken () {
        var color = 0;
        color = map(mouseX,0,width,0,255);
        stroke(color, 150, 255);
        noFill();
        ellipse(this.x,this.y,mouseY/2,mouseY/2);
    }
}
